# Setup

## Discord Bot

- Create new Discord application [here](https://discordapp.com/developers/applications/me)
- Click "new app", choose a name for your application, confirm by clicking "create app"
- Create a Bot User on your app's page
- Open "OAuth URL Generator", choose scope `bot`
- Add permissions: `Manage Channels`, `Manage Webhooks` and whole `Text Permissions` group (or just `Administrator`), then copy link to your browser
- Add your bot to chosen guild(s)
-
# install
-Install Node.js - the engine required to run Miscord

-Required for NPM global and local install

-The latest LTS version of Node.js is recommended.

-If none or low version of Node.js is installed, you need install the latest version of Node.js according to the following instructions:

    For Windows: please visit nodejs.org to download the latest LTS version of Node.js and then, install it using the default options.

    For Mac: the same as Windows.

    For Linux: using package manager to install is recommended, because in this way, you don't need to configure the path. If you fail, you can also use the binary version of Node.js directly.
        using package manager: follow the instructions here
        using binary version: visit official Node.js website to download the latest Linux Binaries (or using command wget to download), unzip (tar -xzvf node-xxx.tar.gz), add the absolute path of bin directory to system PATH after extracting.
        with source package: visit official Node.js website to download the latest version of Source Code (or using wget in shell), unzip (tar -xzvf node-xxx.tar.gz), switch to the root directory (cd node-xxx), execute ./configure, ./make and ./make install in order.

-You can execute node -v in shell to check if the expected version of Node.js is installed successfully:

-$ node -v
-v8.9.4

